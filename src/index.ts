/* eslint-disable */
//---------------------------------------------
//     Function overloads challenge
//---------------------------------------------
/**
 * Implement a function called *random* which returns a random number
 * we can optionaly pass a *max* range to pick from
 * we can optionaly pass a *min* number to start the range
 * we can optionally pass a *float* boolean to indicate if we want a float (true) or an integer (false)
 *   the *default is false* for the float boolean
 * if we call random with *no parameters*, it should *return either 0 or 1*
 * -----------------------
 * implement the function and call it in all variations
 * log the results for the different use cases
 */

function random():number;
function random(flag:Boolean):number;
function random(flag:Boolean, max:number):number;
function random(flag:Boolean, min:number, max: number):number;

function random(arg1?:Boolean, arg2?:number, arg3?: number):number {
    let integer = true;
    if(arg1){
        integer= false;
    }
    let result =  Math.random();
    if(!arg2){//there is no arg2
         result = integer? (result <= 0.5 ? 0 : 1):result;
    }  //there is max
        else if(!arg3){//there is no min
            result = integer?Math.round(result *arg2): result *arg2; 
        }
        else{//there is min and max
            result = integer?Math.round(result*(arg3-arg2)+ arg2) : result*(arg3-arg2)+arg2;
        }   
        return result;
}




const num1 = random(); // 0 ~ 1 | integer
const num2 = random(true); // 0 ~ 1 | float
const num3 = random(false, 6); // 0 ~ 6 | integer,
const num4 = random(false, 2, 6); // 2 ~ 6 | integer
const num5 = random(true, 6); // 0 ~ 6 | float
const num6 = random(true, 2, 6); // 2 ~ 6 | float

console.log({ num1, num2, num3, num4, num5, num6 });

//---------------------------------------------------------------
